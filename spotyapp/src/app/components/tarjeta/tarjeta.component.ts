import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-tarjeta',
  templateUrl: './tarjeta.component.html',
  styleUrls: ['./tarjeta.component.css']
})
export class TarjetaComponent implements OnInit {

  @Input() items: any[] = [];

  constructor( private router: Router) { }

  ngOnInit(): void {
  }

  verArtista( item: any ){

    let codigo;

    if(item.type === 'artist'){
      codigo = item.id;
    }else{
      codigo = item.artists[0].id;
    }

    console.log( codigo );
    this.router.navigate( ['/artist', codigo ] );
  }

}
