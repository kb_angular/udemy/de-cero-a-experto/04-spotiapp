import { Component, OnInit } from '@angular/core';
import { SpotifyService } from '../../services/spotify.service';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

  listArtista: any[] = [];
  loading: boolean;
  constructor(private spotifyService: SpotifyService) { }

  ngOnInit(): void {
  }

  buscar(artist: string) {
    this.loading = true;

    this.spotifyService.getArtists(artist)
      .subscribe( (data: any) => {

        this.listArtista = data;
        this.loading = false;

        console.log(this.listArtista);
    });
  }

}
