import { Component, OnInit } from '@angular/core';
// para capturar datos que se enviar por ruta
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-artist',
  templateUrl: './artist.component.html',
  styleUrls: ['./artist.component.css']
})
export class ArtistComponent implements OnInit {

  constructor(private aRouter: ActivatedRoute) {
    this.aRouter.params.subscribe(params => {
      console.log( params['id'] );
    });
  }

  ngOnInit(): void {
  }

}
