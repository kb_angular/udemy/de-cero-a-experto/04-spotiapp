import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
// para la transformar la data (json)
import { map } from 'rxjs/operators';
import { ConstantPool } from '@angular/compiler';

@Injectable({
  // Con asignacion, ya no se necesita AGREGAR el servicio en providers
  providedIn: 'root'
})
export class SpotifyService {

  constructor(private http: HttpClient) {
    console.log('Soy un servicios - Spotify-App');
   }

  getQuery( query: string) {
    const headers = new HttpHeaders( {
      'Authorization': 'Bearer BQAMF6Hmv8HTiK7FATzDvJndH9k323XOx_XgRAYV6FCZHQw_Vk0T--2EEfMWHpxvwyB4GEXYIS6zP2W-ZnkGlI2p6KtL0ceoID880IDZ0FXXa-vSGCXqqCa-TupoLepRn5MQobi6dQ'
    } );

    const url = `https://api.spotify.com/v1/${query}`;
    return this.http.get( url, { headers });
  }


  getNewRelease() {
    // this.http.get('https://api.spotify.com/v1/browse/new-releases', { headers })
    return this.getQuery('browse/new-releases?limit=10').pipe( map( data => data['albums'].items));
  }

  getArtists(artist: string){
    return this.getQuery(`search?q=${artist}&type=artist&limit=10`).pipe( map( data => data['artists'].items ));
  }
}
