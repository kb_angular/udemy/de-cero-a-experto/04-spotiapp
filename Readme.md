# CONCEPTOS FUNDAMENTALESAPLICACION DE UNA SOLA PAGINA

## Datos fundamentales
    1. Crear una aplicacion angular
        ng new my-app
    
    2. Ejecutar una aplicacion
        ng serve            --> solo inicia la aplicacion
        ng serve -o         --> inicia la aplicacion y abre el navegador
    
    3. Cargar una aplicacion angular, tenemos que instalar el node_moudeles
        npm install
    
    4. Crear un componente
        ng g c components/header

    5. Creando un Servicio
        ng g s services/spotify
    
    6. Creando un pipe
        ng g p pipes/capitalizado


## Formas de instalar el Bootstrap en nuestra aplicion
    1.  CDN
        https://getbootstrap.com/docs/4.5/getting-started/download/

    2.  Descargando las cartepas
        Descargado la carpeta bootstrap, este debe estar alojado en 'assets' de nuestra aplicacion Angular
    
    3.  Instalando en npm
        `npm install bootstrap`     --> Lo podemos utilizar desde la carpeta del modulo de node.


## Formas de llamar los parametros en html
    {{ heroe.nombre }}      --> Interpolacion
    [src] = 'heroe.img'     --> para que angular asigne la ubicacion. 
        
## Rutas
    Navegar en las pestanas sin hacer refresh de la aplicacion, solo debe cargar lo que debe mostrar

    creamos el archivo `app.route.ts`
    dentro del archivo escribimos la palabra reservada `ngroute....`  +  enter
    este archivo creado desbe estar instanciado en el archivo app.modules.ts

    <router-outlet></router-outlet>    --> Para llamar a una ruta

    routerLinkActive="active"   --> para especificar la posicion del puntero, se coloca en todo, estara junto con [routerLink]
    [routerLink]="['home']"     --> para hacer una llamado directo, angualar toma el control


    
### Ruta: Para moverno de una pagina, ENNVIANDO parametro
    En el app.routes.ts
    { path: 'heroe/:id', component: ArtistComponent }
    ...

    Directo
    [routerLink]="['/herore', i]"     --> especificar la ruta, con parametro

    Con funcion
        (click)="verHerore(i)"
    En el componente importamos 'Router', y hacemos desde el constructor
        verHeroe(idx:number){
            this.router.navigate( ['/heroe', idx] ) 
        }

### Ruta: Recibiendo Parametro
    Importamos desde componente en curso 'ActivatedRouter',  y llamamos desde el constructor
    Devuelve un 'Observable', para lo cual si queremos hacer uso tenemos que 'Suscribirnos'

    constructor(... ) {
        this.activatedRouter.params.subribe( param => {
            console.log( param['id']):
            this.heroe = this._heroesService.getheroe( param['id'] );

        })
    }

    *** id --> nombre del parammetro declarado en el router.
    


## Servicios
    Los servicios tienen la funcionalidad de:
        - Brindar informacion a quien lo necesite
        - Realizar peticiones CRUD
        - Mantener la data de forma persitente
        - Servir como recurso re-utilizable para nuestra aplicacion.
    
    La creacion del servicio, tiene que estar instanciada en 'app.module.ts' en la seccion 'providers'


    ngserv... --> Nos autocompleta, para crear la estructura del servicio

    Para usar un servicio, tenemos que importarla, y llamarla desde un contructor del componente.

### Interfaz en Servicio
    Las interfaces se crean para no manipular las propiedades de una objeto.
    Se declara solo sus propiedades con su tipo de datos.


## @Input
    Recibe info desde afuera.
    PADRE(tarjeta)
    HIJO (Los comp. ve van a consumir del padre)

    Importamos (tarjeta)
    Input

    En el COmponente (tarjeta)
        @Input() heroe:any = {}
        heroe --> indicamos que su valor vendra desde afuera.
    
    En el HTML del otro componente
        <app-tarjeta [heroe]="heroe" *ngfor="let heroe of heroes"><app-tarjeta>
        [heroe] --> nombre de la propiedad de componente TARJETA
        "heroe" --> lo que envia desde el bucle.
    

## @Output y EventEmitter
    PADRE(tarjeta)
    HIJO (Los comp. ve van a consumir del padre)

    Importamos (tarjeta)
    Output
    EventEmitter

    En el COmponente (tarjeta), lo que queremos emitir es un entero.

        @Input() heroe: any = {}
        @Input() index: number;

        @Output() heroeSeleccionado: EventEmitter<number>;
        

    En el constructor (...) {
        //inicializamostarjeta)
        this.heroeSeleccionado = new EventEmitter();        

    en un metodo (tarjeta)
    verHereo(){
        this.heroeSeleccionado.emit( this.index );
    }
    
    En el HTML del otro componente
        <app-tarjeta (heroeSeleccionado)="verHeroe( $event)" [heroe]="heroe" *ngfor="let heroe of heroes"><app-tarjeta>
        heroeSeleccionado --> es el mismo nombre que se declarado en tarjeta
        [heroe] --> nombre de la propiedad de componente TARJETA
        "heroe" --> lo que envia desde el bucle.

    En el otro componente
        verHeroe( idx: number) {
            this.router.navigate( ['/heroe', idx] );
        }
   
## Pipe


    Nos permite transformar la informacion de manera visual, sin alterar la data original.
    Normalmente se trabaja del lado HTML.

    Alguno pipes:
        -   Pipes uppercase y lowercase
        -   Pipe Slice
        -   Pipe Decimal
        -   Pipe Percent
        -   Pipe Currency
        -   Pipe Json
        -   Pipe Async
        -   Pipe Date
        -   Pipes Personalizados
            a- Capilizar palabras y nombres
            b- Creacion de un pipe, que termine cargar recursos externos de forma segura.

    https://angular.io/api/core/PipeTransform
    

###  Pipe date
    Para que la fecha nos muestre en el idioma deseado, ejecutamos lo siguiente:
    ng add @angular/localize 

    Importamos
    import { LOCALE_ID } from '@angular/core'

    Importamos " registerLocaleData "
    import { registerLocaleData } from '@angular/common'
    import localEs from '@angular/common/locales/es';

    registerLocaleData(LocalEs)



    En Providers: [
        {
            provide: LOCALE_ID,
            useValue: 'es'
        }
    ]

### Creaando un Pipe personalizado
    Capitalizable
    ng g p pipes/capitalizado

    Para insertar Video
    Para insertar un elemento externo, ejemplo URL, debemos crear un pipe, para darle la seguridad que es confiable.

    ng g p pipes/domseguro


### Pipe de Imagenes
    ng g p pipe/noimage --spec=false
    Lo que voy a recibir del servicio es un arreglo de imagenes, y pipe devolvera un string
    
        transform(image: any[]): string {
            if( !image ){
                return 'assets/img/noimage.png';
            }

            if( image.length > 0 ){
                return image[0].url;    
            }else{
                return 'assets/img/noimage.png';
            }
        }
    
    En el HTML del otro componente, utilizamos el pipe
    Antes
        [src]="artista.images[0].url"
    Ahora
        [src]="artista.images | noimage"
    
    noimage --> nombre del pipe

#   SPOTIFY APP
    Aplicacion que consume servicio de Spotyfy

    Para obtener el token de Spotify
    https://developer.spotify.com/dashboard/
    

    Para Usar la API disponibles
    https://developer.spotify.com/console/

    

##  Peticiones HTTP
    
    API
    https://restcountries.eu/

    Recuperando informacion: Paises que hablan espanol
    https://restcountries.eu/rest/v2/lang/es

    
    Primero en app.module.ts
    import { HttpClientModule } from '@angular/common/http';
    ...
    imports: [
        HttpCLientModule
    ]



    EN EL SERVCICIO, importamos HttpCLient
    import { HttpClient, HttpHeaders } from '@angular/common/http';

    En el constructor
    contructor( private http: HttpClient ) {
        ...
    }

    
    getRelease() {

        const headers = new HttpHeaders({
            'Authorization': 'Bearer xxxxxx0';
        })
        //la suscripcion se realizara en el componente
        return this.http.get('URL', { headers });
    }


    EN EL COMPONENTE, importamos el servicio y consumimos    
    constructor( private spotify: SpotifyService ) {
        this.spotify.getNewReleases().suscribe( data -> 
            console.log( data );
        )
    }


# Eventos en Angular
    (click) = buscar() --> cuando hace click sobre un elemento
    (keyup) = buscar() --> cuando presiona una tecla o suelta    

# Operadores en Angular
    Map --> Toma la informacion y la transforma
    
    El operador map lo trabajamos desde el servicio, para ello importamos
    import { map } from 'rxjs/operators';

    para utilizar el operador map, trabajamaos con pipe()
    getArtists(artist: string){
        return this.getQuery(`search?q=${artist}&type=artist&limit=10`)
                    .pipe( map( data => data['artists'].items ));
    }
    Enmarcamos la propiedad [artists] para que la app 'data', busque una propiedad llamada artists


# DATOS ADICIONALES
    Para agregar un elemento a una coleccion
        heroesArr.push(heroe):
    
    split -->  convierte en un arreglo, a cada elemento despues despues de un spacio.
    let nombres = value.split(' ');

    nombre[0].toLocaleUpperCase() --> mayuscula la primera letra del string
    nombre.substr(1)  --> obtiene a partir de la indice 1

    unimos un arreglo es un solo string
    return nombres.join(' ');


    Automaticamente instanciada en app.module.ts
    En el componente se debe declarar lo siguiente (actualmente es automatico)
    @Injectable({
        providedIn: 'root'
    })

    Capturando la REFERENCIA LOCAL DE UN INPUT -> #termino
    <input #termino type="text" (keyup)="buscar(termino.value)" class="...>

    AGREGAR PARAMETROS sobre una cadena de texto
    `search?q=${artist}&type=artist&limit=10`

    
## Set de Iconos
    
    Trabajemos con el CDN
    https://fontawesome.com/




    